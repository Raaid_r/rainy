﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour {

    /*
    * 
    *  Author - Raaid
    * 
    */

    //This script is put on buttons etc. to trigger the main dialogue windows I made (inc, animations)
    public Dialogue dialogue;

    public void TriggerDialogue()
    {
        //If theres nothing in the item slot, don't trigger dialogue - Tony
        if (!dialogue.name.Equals(""))
        {
            FindObjectOfType<DialogueManager>().StartDialogue(dialogue, 0);
        }
    }


    //These functions are used to show the item name when mouse is hovered
    public void OnTriggerName()
    {
        if (!dialogue.name.Equals(""))
        {
            FindObjectOfType<DialogueManager>().OnHover(dialogue.name);
        }
    }

    public void OffTriggerName()
    {
        if (!dialogue.name.Equals(""))
        {
            FindObjectOfType<DialogueManager>().OffHover();
        }
    }
}
