﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue {

    /*
    * 
    *  Author - Raaid
    * 
     */

    //This is a scriptable object for dialogue
    
    public string name;
    public string[] sentences;



}
