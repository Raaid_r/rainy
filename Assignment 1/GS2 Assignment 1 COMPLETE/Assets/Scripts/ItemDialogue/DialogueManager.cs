﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

    /*
     * 
     *  Author - Raaid
     * 
     */

    public Text dialogueText;
    public Text nameText;
    public Animator animatorName;
    public Animator animatorDesc;
    public bool isClicked;
    private Queue<string> sentences;
    private Time delayTime;

	void Start ()
    {
        sentences = new Queue<string>();

    }
    

    public void StartDialogue(Dialogue dialogue, float delay)                            //Fuction to start dialogue an animations
    {
        isClicked = true;                                   //Variable to make sure name box doesn't disappear when clicked
        Debug.Log("Starting convo");

        if (dialogue.name == "Text")                        //Statement to only show item name when an item is clicked, not a conversation
        {
            animatorName.SetBool("IsOpen", false);
        }
        else
        {
            animatorName.SetBool("IsOpen", true);
        }

        animatorDesc.SetBool("IsOpen", true);
       
        sentences.Clear();

        nameText.text = dialogue.name;

        foreach (string sentence in dialogue.sentences)     //Queues each string into the queue - FIFO
        {
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }


    public void DisplayNextSentence()                                       //Function which checks the queue for incoming sentences otherwise ends dialogue
    {
        Debug.Log("Testing");
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }
        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));     //Starts TypeSentence Coroutine
    }

    IEnumerator TypeSentence (string sentence)                          //Coroutine which creates scrolling text by converting the sentence string
    {                                                                   //into a char array and prints each character individually
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }


    void EndDialogue()                                                  //Function which ends the dialogue and plays exit animations
    {
        Debug.Log("End of convo");
        animatorName.SetBool("IsOpen", false);
        animatorDesc.SetBool("IsOpen", false);
        isClicked = false;
    }

        
    public void OnHover(string name)                                    //Functions used for mouse hover to play name animation
    {
        if (!isClicked)                                                 //IsClicked variable makes sure item name doesnt disappear when
        {                                                               //the item is clicked from the inventory
            nameText.text = name;
            animatorName.SetBool("IsOpen", true);
        }
    }

    public void OffHover()
    {
        if (!isClicked)                                                 //IsClicked variable makes sure item name doesnt disappear when
        {                                                               //the item is clicked from the inventory
            animatorName.SetBool("IsOpen", false);
        }
    }

}
