# Rainy Studios

----------------------------------------------------------------------------------------------
__Raaid__ 
S3719919 | __Tony__ 
S3721573	


__Assignment 1:__

Tony - _Worked on scrolling text and getting text to display correct items description and name._

Raaid - _Created animations, buttons and textboxes for text to be displayed on._




__Assignment 2:__

Tony - _Worked on item dropping, drop button, new items (banana and dirty banana), new 
interactable (dirty banana)._

Raaid - _Created new inventory screen, new interactions with new items and new interaction with 
fruit vendor, changed fish vendor reactions to change objective to obtain items._




__Assignment 3:__

Tony worked on:

*	Ball firing and bouncing within a confined space and destroying blocks.
*	Input and control system.
*	Logic behind high score, ball count, coin count.
*	Logic behind unlocking balls
*	Saving data
*	Block Animation
*	Block Visual Feedback
*	Implemented collectables such as coin, powerup and invincibility
*	Implemented fast forward option


Raaid worked on:

*	New blocks appear after each turn.
*	Game over condition when any block touches the floor.
*	Blocks have additional health as game progresses
*	Menu
*	Aesthethic of shop
*	Minimap
*	Music
*	UI Displaying high score, ball count, coin count.
*	UI for game over
