﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */
 [RequireComponent(typeof(BallMovement))]
public class Balls : MonoBehaviour
{
    [SerializeField]
    private GameObject _ballPrefab;
    [SerializeField]
    private int _startAmount;
    [SerializeField]
    [Range(0, 5)]
    private float _cycleSpeed;

    private Color[] _colors = { Color.red, Color.yellow, Color.green, Color.blue, Color.cyan };
    private List<GameObject> _balls;
    private int _amount;
    private static bool _isInvincible;
    private int _currentAmount;

    [SerializeField]
    private TMP_Text _text;

    public static bool IsInvincible
    {
        get { return _isInvincible; }
    }

    public int Amount
    {
        get { return _amount; }
    }

    public GameObject[] AllBalls
    {
        get { return _balls.ToArray(); }
    }

    private void Awake()
    {
        _amount = 0;
        _balls = new List<GameObject>();
        //set the start amount of balls
        for (int i = 0; i < _startAmount; i++)
        {
            AddBall(); 
        }
    }

    private void Start()
    {
        Invincibility.OnCollect += SetInvincibility;
        PowerUp.OnCollect += AddBall;
        BallMovement.onBallShot += UpdateUI;
    }

    private void OnDestroy()
    {
        Invincibility.OnCollect -= SetInvincibility;
        PowerUp.OnCollect -= AddBall;
        BallMovement.onBallShot -= UpdateUI;
    }

    private void UpdateUI()
    {
        _currentAmount--;
        _text.text = _currentAmount.ToString();
    }

    private void SetInvincibility()
    {
        _isInvincible = true;
    }

    private void Update()
    {
        //Does the colour change of the balls when its invincible
        if(!GetComponent<BallMovement>().IsMoving)
        {
            _isInvincible = false;
            _currentAmount = _amount;
            _text.text = _currentAmount.ToString();
            foreach (GameObject ball in _balls)
            {
                ball.GetComponent<Renderer>().material = PersistentManager.Player.SkinTexture;
            }
        }
        if (IsInvincible)
        {
            CycleRainbow();
        }
    }

    public void AddBall(PowerUp powerUp = null)
    {
        GameObject ball;
        //create a new ball and set the speed to 0
        if (powerUp == null)
        {
            ball = Instantiate(_ballPrefab);
        }
        else
        {
            ball = Instantiate(_ballPrefab, powerUp.transform.position, Quaternion.identity);
            ball.GetComponent<Collider>().enabled = false;
            ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 5, 0);
            StartCoroutine(Drop(ball, new Vector3(ball.transform.position.x, 0, ball.transform.position.z)));
        }
        _balls.Add(ball);

        //ignore collisions of other balls
        for (int i = 0; i < _balls.Count; i++)
        {
            if(i == _amount)
            {
                break;
            }
            Physics.IgnoreCollision(_balls[_amount].GetComponent<Collider>(), _balls[i].GetComponent<Collider>());
        }   
        _amount++;
    }

    private void CycleRainbow()
    {
        float scaledTime = Mathf.PingPong(Time.time * _cycleSpeed, 1) * (_colors.Length - 1);
        Color oldColor = _colors[(int)scaledTime];
        Color newColor = _colors[(int)(scaledTime + 1)];
        float newT = scaledTime - Mathf.Round(scaledTime);
        foreach (GameObject ball in _balls)
        {
            if (ball.GetComponent<Rigidbody>().velocity.magnitude > 0 && ball.GetComponent<Collider>().enabled)
            {
                ball.GetComponent<Renderer>().material.color = Color.Lerp(oldColor, newColor, newT);
            }
        }
    }

    private IEnumerator Drop(GameObject ball, Vector3 endPosition)
    {
        //When powerups are picked up, instantiate and drop the ball from the power up
        float elapseTime = 0;
        while (Vector3.Distance(ball.transform.position, endPosition) > 1f)
        {
            elapseTime += Time.deltaTime;
            ball.GetComponent<Rigidbody>().AddForce(Vector3.down * 20 * ball.GetComponent<Rigidbody>().mass);
            yield return null;
        }
        ball.GetComponent<Collider>().enabled = true;
    }
}
