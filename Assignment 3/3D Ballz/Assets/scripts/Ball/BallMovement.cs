﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */
[RequireComponent(typeof(Balls))]
public class BallMovement : MonoBehaviour {

    [SerializeField]
    private float _fireRate;
    [SerializeField]
    private float _speed;

    private Vector3 _velocity;
    private int _ballsShot;
    private float _time;
    private GameObject[] _balls;
    private float _timeScale;

    public delegate void BallShotHandler();
    public static event BallShotHandler onBallShot;

    public bool IsMoving
    {
        //checks if any of the balls are still moving
        get
        {
            foreach (GameObject ball in GetComponent<Balls>().AllBalls)
            {
                if (ball.transform.position != Vector3.zero)
                {
                    return true;
                }
            }
            return false;
        }
    }

    public bool IsAllBallsShot
    {
        get
        {
            if (_ballsShot == _balls.Length)
            {
                return true;
            }
            return false;
        }
    }

    private void Start()
    {
        _balls = GetComponent<Balls>().AllBalls;
        _time = 1 / _fireRate;
        _ballsShot = _balls.Length;
    }

    public void Move(Vector3 direction)
    {
        //Player cannot shoot while balls are moving
        if (!IsMoving)
        {
            _velocity = direction * _speed;
            _time = 1 / _fireRate;
            _balls = GetComponent<Balls>().AllBalls;
            _ballsShot = 0;
        }
    }

    public void FixedUpdate()
    {
        Vector3 velocity;
        foreach(GameObject ball in _balls)
        {
            velocity = ball.GetComponent<Rigidbody>().velocity;
            if(velocity.magnitude > 0 && velocity.magnitude < _speed)
            {
                ball.GetComponent<Rigidbody>().velocity = Vector3.Normalize(velocity) * _speed;
            }
        }
        _time += Time.deltaTime;
        //fire the balls until all balls are shot
        if (_time > 1 / _fireRate)
        {
            if(!IsAllBallsShot)
            {
                _balls[_ballsShot].GetComponent<Rigidbody>().velocity = _velocity;
                _time = 0;
                _ballsShot++;
                if (onBallShot != null)
                {
                    onBallShot();
                }
            }
        }
        //ensures that the balls do not get stuck
        foreach (GameObject ball in _balls)
        {
            if (Mathf.Abs(ball.GetComponent<Rigidbody>().velocity.y) < 1 && ball.GetComponent<Rigidbody>().velocity.magnitude > 0)
            {
                ball.GetComponent<Rigidbody>().velocity += new Vector3(0f, -1f, 0f);
            }
        }
    }
}
