﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */

public class Cube : MonoBehaviour
{
    private Color _color;
    private int _hp;
    [SerializeField]
    private Text text;

    public delegate void CubeDeathHandler(Cube cube);
    public static event CubeDeathHandler OnDeath; 

    public void Start()
    {
        UpdateColour();
    }

    public int Hp
    {
        get
        {
            return _hp;
        }
    }

    public void SetHealth(int hp)
    {
        //When setting the health, update the colour
        _hp = hp;
        UpdateColour();
        text.text = _hp.ToString();
    }

    public void Hit(int damage)
    {
        //Lowers the cubes health, and die if its below 0 health
        _hp -= damage;
        text.text = _hp.ToString();
        if (_hp <= 0)
        {
            gameObject.SetActive(false);
            OnDeath(this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //If the balls are invincible, die instantly. Else, take one damage
        if (Balls.IsInvincible)
        {
            Hit(_hp);
        }
        Hit(1);
        //Update colour when damaged
        UpdateColour();
    }

    private void UpdateColour()
    {
        //Change between colour depends on health
        if (_hp <= 5)
        {
            _color = Color.cyan;
        }
        else if (_hp <= 15)
        {
            _color = Color.Lerp(Color.cyan, Color.blue, (_hp - 5) / 10f);
        }
        else if (_hp <= 25)
        {
            _color = Color.Lerp(Color.blue, Color.green, (_hp - 15) / 10f);
        }
        else if (_hp <= 55)
        {
            _color = Color.Lerp(Color.green, Color.yellow, (_hp - 45) / 10f);
        }
        else
        {
            _color = Color.Lerp(Color.yellow, Color.red, (_hp - 65) / 10f);
        }
        gameObject.GetComponent<Renderer>().material.color = _color;
    }
}
