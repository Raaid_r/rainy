﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour {

    [SerializeField]
    private GameObject _cubeParticleEffect;
    [SerializeField]
    private GameObject _powerUpParticleEffect;

	void Start ()
    {
        Cube.OnDeath += CreateCubeParticle;
        PowerUp.OnCollect += CreatePowerUpParticle;
	}

    private void OnDestroy()
    {
        Cube.OnDeath -= CreateCubeParticle;
        PowerUp.OnCollect -= CreatePowerUpParticle;
    }

    private void CreateCubeParticle(Cube cube)
    {
        Instantiate(_cubeParticleEffect, cube.transform.position, Quaternion.Euler(90, 0, 0));
    }

    private void CreatePowerUpParticle(PowerUp powerUp)
    {
        Instantiate(_powerUpParticleEffect, powerUp.transform.position, Quaternion.Euler(90, 0, 0));
    }
}
