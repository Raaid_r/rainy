﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;



public class ScoreManager : MonoBehaviour {

    [SerializeField]
    private LevelManager _levelManager;
    [SerializeField]
    private TMP_Text _text;
    [SerializeField]
    private TMP_Text _endScore;
    [SerializeField]
    private TMP_Text _highScore;
    [SerializeField]
    private TMP_Text _coins;

    private int _score;

    private void Start()
    {
        Coin.OnCollect += UpdateCoins;
        _coins.text = PersistentManager.Player.Coins.ToString();
        _highScore.text = PersistentManager.Player.GetHighScore(0).ToString();
    }

    private void OnDestroy()
    {
        Coin.OnCollect -= UpdateCoins;
    }

    public void UpdateScore()
    {
        _score = _levelManager.Level;
        PersistentManager.Player.UpdateHighScore(0, _score);
        _text.text = _score.ToString();
        _endScore.text = _score.ToString();
        _highScore.text = "BEST"+ "\n" + PersistentManager.Player.GetHighScore(0).ToString();
    }

    private void UpdateCoins(int amount)
    {
        _coins.text = PersistentManager.Player.Coins.ToString();
    }
}
