﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

    [SerializeField]
    private int _level;
    [SerializeField]
    private BallMovement _ballMovement;
    [SerializeField]
    private Transform _objectArea;
    [SerializeField]
    private SpawnObjects _spawnObjects;
    [SerializeField]
    private ScoreManager _scoreManager;
    [SerializeField]
    private Animator animator;


    private Vector3 _position;


    //delegates and event system done by Tony
    public delegate void LevelHandler(int level);
    public static event LevelHandler OnLevelUp;


    //Sets start level to 0 
	public void Start ()
    {
        _level = 0;
        animator.SetBool("IsOpen", false);

    }
	
    //Method for updating score and calling coroutine to lower blocks
    public void NextLevel()
    {
        if (!_ballMovement.IsMoving)
        {
            Debug.Log("Next Level");
            _level++;
            _scoreManager.UpdateScore();
            StartCoroutine(MoveDown(_objectArea, 0.2f));
            _spawnObjects.Spawn();
        }
    }

    //Getter for level
    public int Level
    {
        get
        {
            return _level;
        }
    }

    //Method for ending game and opening end screen
    public void EndGame()
    {
        Debug.Log("GameOver");
        animator.SetBool("IsOpen", true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    //Animation enumerator for block lowering animation
    private IEnumerator MoveDown(Transform thing, float timeLength)
    {
        float elapseTime = 0;
        Vector3 startPosition = thing.position;
        Vector3 endPosition = thing.position + Vector3.down;
        while (elapseTime < timeLength)
        {
            elapseTime += Time.deltaTime;
            thing.position = Vector3.Lerp(startPosition, endPosition, elapseTime / timeLength);
            Debug.Log(elapseTime);
            yield return null;
        }
        Debug.Log(elapseTime);
        thing.position = endPosition;
    }

}
