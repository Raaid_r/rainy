﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateManager : MonoBehaviour {
    [SerializeField]
    private GameObject _walls;
    [SerializeField]
    private GameObject _minimap;

    private void Update()
    {
        RotateEnvi();
    }

    //Method for rotating minimap
    //Secondary camera is hooked up to the walls
    //The walls and camera rotate while game objects stay in their orientation
    //giving multiple angles of view
    public void RotateEnvi()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            _walls.transform.Rotate(Vector3.up * 90);
            _minimap.transform.Rotate(Vector3.up * 90);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            _walls.transform.Rotate(Vector3.up * -90);
            _minimap.transform.Rotate(Vector3.up * -90);
        }
    }






}
