﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Skin : MonoBehaviour {

    [SerializeField]
    private Material _material;
    private bool _isUnlock;

    private int _ID;

    public int ID
    {
        get { return _ID; }
    }

    public Material Material
    {
        get { return _material; }
    }

    public bool IsUnlock
    {
        get { return _isUnlock; }
    }

    private void Awake()
    {
        _isUnlock = false;
    }

    private void Start()
    {
        //Set the material to be solid black if it is locked
        Material material = new Material(Shader.Find("Diffuse"));
        material.color = Color.black;

        if(!_isUnlock)
        {
            GetComponent<Renderer>().material = material;
        }
    }

    private void Update()
    {
        transform.Rotate(Vector3.up * Time.deltaTime * 50);
    }

    public void SetSkin(Material material, int ID)
    {
        //Set the skin and ID, and if theres already a material, it cannot be changed
        if(_material != null)
        {
            Debug.Log("Skin already set");
        }
        else
        {
            _material = material;
            _ID = ID;
        }
        if(PlayerPrefs.GetInt("Skin" + _ID) == 1)
        {
            Unlock();
        }
    }
    public void Unlock()
    {
        //unlocks the skin and saves it
        _isUnlock = true;
        GetComponent<Renderer>().material = _material;
        PlayerPrefs.SetInt("Skin" + _ID, 1);
    }


}
