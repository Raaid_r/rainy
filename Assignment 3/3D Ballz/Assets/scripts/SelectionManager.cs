﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public enum ButtonType { Selection = 0, Left, Right };

public class SelectionManager : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private Button[] _buttons;
    [SerializeField]
    private TMP_Text _coinAmount;
    [SerializeField]
    private GameObject _shop;
    [SerializeField]
    private GameObject _menu;
    private int _currentSkin;


    private void Start()
    {
        _coinAmount.text = PersistentManager.Player.Coins.ToString();
        _currentSkin = PlayerPrefs.GetInt("CurrentSkin");
        _camera.transform.position = new Vector3(_currentSkin, 0, -3);
        UpdateSelectButton();
    }

    public void Move(int distance)
    {
        if (_currentSkin + distance >= 0 && _currentSkin + distance < ShopManager.Shop.SkinAmount)
        {
            _currentSkin += distance;
            StartCoroutine(Move(0.2f, distance));
        }
        UpdateSelectButton();
    }

    public void Select()
    {
        if(ShopManager.Shop.IsUnlock(_currentSkin))
        {
            ShopManager.Shop.Select(_currentSkin);
        }
        else
        {
            ShopManager.Shop.Buy(_currentSkin);
            _coinAmount.text = PersistentManager.Player.Coins.ToString();
        }
        UpdateSelectButton();
    }

    public void UpdateSelectButton()
    {
        if (ShopManager.Shop.IsSelected(_currentSkin))
        {
            _buttons[(int)ButtonType.Selection].interactable = false;
            _buttons[(int)ButtonType.Selection].GetComponentInChildren<TMP_Text>().text = "Selected";
        }
        else if (ShopManager.Shop.IsUnlock(_currentSkin))
        {
            _buttons[(int)ButtonType.Selection].interactable = true;
            _buttons[(int)ButtonType.Selection].GetComponentInChildren<TMP_Text>().text = "Select";
        }
        else
        {
            _buttons[(int)ButtonType.Selection].interactable = PersistentManager.Player.CanSpend(ShopManager.Shop.Price)?true:false;
            _buttons[(int)ButtonType.Selection].GetComponentInChildren<TMP_Text>().text = ShopManager.Shop.Price.ToString() + " coin";
        }
    }

    public void Return()
    {
        _shop.SetActive(false);
        _menu.SetActive(true);
    }

    public void GoTo()
    {
        _menu.SetActive(false);
        _shop.SetActive(true);
    }

    private IEnumerator Move(float timeLength, int distance)
    {
        float elapseTime = 0;
        Vector3 startPosition = _camera.transform.position;
        Vector3 endPosition = startPosition + new Vector3(distance, 0, 0);
        _buttons[(int)ButtonType.Left].interactable = false;
        _buttons[(int)ButtonType.Right].interactable = false;
        while(elapseTime < timeLength)
        {
            elapseTime += Time.deltaTime;
            _camera.transform.position = Vector3.Lerp(startPosition, endPosition, elapseTime / timeLength);
            yield return null;
        }
        _camera.transform.position = endPosition;
        _buttons[(int)ButtonType.Left].interactable = true;
        _buttons[(int)ButtonType.Right].interactable = true;
    } 
}
