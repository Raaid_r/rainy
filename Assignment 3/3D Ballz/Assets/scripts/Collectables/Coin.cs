﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : Collectable
{
    /*  Author: Tony Anh Nguyen
    *  Student Number: s3721573
    */
    public delegate void CollectableHandler(int amount);
    public static event CollectableHandler OnCollect;

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (OnCollect != null && other.CompareTag("Ball"))
        {
            OnCollect(1);
        }  
    }
}
