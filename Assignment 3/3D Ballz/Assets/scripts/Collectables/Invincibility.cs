﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */

public class Invincibility : Collectable
{
    [SerializeField]
    [Range(0, 5)]
    private float _cycleSpeed;

    private Color[] _colors = { Color.yellow, Color.green, Color.red, Color.blue, Color.cyan };

    public delegate void CollectableHandler();
    public static event CollectableHandler OnCollect;

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (OnCollect != null)
        {
            OnCollect();
        }
    }

    protected override void Update()
    {
        //Cycle through the colours
        float scaledTime = Mathf.PingPong(Time.time * _cycleSpeed, 1) * (_colors.Length - 1);
        Color oldColor = _colors[(int)scaledTime];
        Color newColor = _colors[(int)(scaledTime + 1)];
        float newT = scaledTime - Mathf.Round(scaledTime);
        gameObject.GetComponent<Renderer>().material.color = Color.Lerp(oldColor, newColor, newT);
        base.Update();
    }
}
