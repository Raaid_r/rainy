﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */

public class PowerUp : Collectable
{
    [SerializeField]
    [Range(1, 5)]
    private float _sizeSpeed;
    public delegate void CollectableHandler(PowerUp powerUp);
    public static event CollectableHandler OnCollect;

    protected override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (OnCollect != null && other.CompareTag("Ball"))
        {
            OnCollect(this);
        }
    }

    protected override void Update()
    {
        base.Update();
        transform.localScale = new Vector3(0.1f, Mathf.PingPong(Time.time * _sizeSpeed, 0.5f) + 0.5f, Mathf.PingPong(Time.time * _sizeSpeed, 0.5f) + 0.5f);
    }
}
