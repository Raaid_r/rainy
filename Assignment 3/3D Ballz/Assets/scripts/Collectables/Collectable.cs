﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */

public abstract class Collectable : MonoBehaviour
{
    [SerializeField]
    protected float Speed;

    protected virtual void Update()
    {
        transform.Rotate(new Vector3(0, Speed, Speed) * Time.deltaTime);
    }

    protected virtual void OnTriggerEnter(Collider other)
    {
        gameObject.SetActive(false);
    }

}
