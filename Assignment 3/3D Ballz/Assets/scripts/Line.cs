﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*  Author: Tony Anh Nguyen
 *  Student Number: s3721573
 */

public class Line : MonoBehaviour {

    [SerializeField]
    [Range(1, 5)]
    private float _colourSpeed;

    private void Update ()
    {
        GetComponent<LineRenderer>().startColor = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time * _colourSpeed, 1));
        GetComponent<LineRenderer>().endColor = Color.Lerp(Color.white, Color.black, Mathf.PingPong(Time.time * _colourSpeed, 1));
    }
}
