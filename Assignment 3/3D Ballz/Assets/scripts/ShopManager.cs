﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    public static ShopManager Shop { get; private set; }

    [SerializeField]
    private Material[] _materials;
    [SerializeField]
    private GameObject _ballPrefab;
    [SerializeField]
    private GameObject[] _balls;
    [SerializeField]
    private Transform _parent;
    [SerializeField]
    private int _price;

    public GameObject[] Balls
    {
        set { _balls = value; }
    }

    public Material GetMaterial(int index)
    {
        return _materials[index];
    }

    public int Price
    {
        get { return _price; }
    }

    public int SkinAmount
    {
        get { return _materials.Length; }
    }
	
	private void Awake () 
    {
        if (Shop == null)
        {
            Shop = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        //instantiate all the balls of the given materials and give it a skin
        _balls = new GameObject[_materials.Length];
        for (int i = 0; i < _balls.Length; i++)
        {
            _balls[i] = Instantiate(_ballPrefab, new Vector3(i, 0, 0), Quaternion.identity);
            _balls[i].AddComponent<Skin>().SetSkin(_materials[i], i);
        }
        _balls[0].GetComponent<Skin>().Unlock();
        ShopManager.Shop.Balls = _balls;
	}

    public bool IsUnlock(int index)
    {
        //Checks if the ball is unlocked
        if(_balls[index].GetComponent<Skin>().IsUnlock)
        {
            return true;
        }
        return false;
    }

    public bool IsSelected(int index)
    {
        //Checks if the ball is already selected
        if(PersistentManager.Player.SkinEquals(_balls[index].GetComponent<Skin>()))
        {
            return true;
        }
        return false;
    }

    public void Select(int index)
    {
        //Sets the skin of the player
        PersistentManager.Player.SetSkin(_balls[index].GetComponent<Skin>());
    }

    public void Buy(int index)
    {
        //Buys the skin and select it
        PersistentManager.Player.SpendCoins(_price);
        _balls[index].GetComponent<Skin>().Unlock();
        Select(index);
    }
}
