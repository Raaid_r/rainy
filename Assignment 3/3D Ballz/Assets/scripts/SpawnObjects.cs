﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects : MonoBehaviour {

    [SerializeField]
    private GameObject _cubePrefab;

    [SerializeField]
    private GameObject _coinPrefab;

    [SerializeField]
    private GameObject _invinPrefab;

    [SerializeField]
    private GameObject _powerPrefab;

    [SerializeField]
    private LevelManager _levelManager;

    [SerializeField]
    private Transform _objectArea;

    private Vector3 _position;

    [SerializeField]
    private int _defaultHeight;
    private int _cubeCount;

    [SerializeField]
    private float _cubeChance, _coinChance, _invincibilityChance, _powerUpChance;

    void Start ()
    {
        _cubeCount = 0;        
    }

    public void Spawn()
    {
        GameObject temp;
        float total = 0;

        //Loop for spawning objects
        for (int i = -2; i <= 2; i++)
        {
            for (int j = -2; j <= 2; j++)
            {
                //Chooses a random number between 1 and 100
                float randomNumb;
                randomNumb = Random.Range(0f, 100f);

                //Sets position for spawning
                _position = new Vector3(i, _defaultHeight, j);

                //Spawns item depending on random number before
                //Essentially turns spawning into a percentage chance
                if (randomNumb < (total += _cubeChance))    
                {
                    temp = Instantiate(_cubePrefab, _position, Quaternion.identity, _objectArea);                    
                    temp.GetComponent<Cube>().SetHealth(Random.Range(_levelManager.Level, _levelManager.Level * 2));
                    _cubeCount++;
                }
                else if (randomNumb >= total && randomNumb < (total += _coinChance))
                {
                    temp = Instantiate(_coinPrefab, _position, Quaternion.identity, _objectArea);                                    
                }
                else if (randomNumb >= total && randomNumb < (total += _powerUpChance))
                {
                    temp = Instantiate(_powerPrefab, _position, Quaternion.identity, _objectArea);
                }
                else if (randomNumb >= total && randomNumb < (total += _invincibilityChance))
                {
                    temp = Instantiate(_invinPrefab, _position, Quaternion.identity, _objectArea);
                }
                total = 0;
            }
        }
        
        //Makes sure there is always at least 1 cube in the game on each level
        if (_cubeCount == 0)
        {
            temp = Instantiate(_cubePrefab, _position, Quaternion.identity);
            temp.GetComponent<Cube>().SetHealth(Random.Range(_levelManager.Level, _levelManager.Level * 2));
            _cubeCount++;
        }
    }
}
