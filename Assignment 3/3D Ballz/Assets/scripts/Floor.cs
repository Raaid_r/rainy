﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Floor : MonoBehaviour {

    /*  Author: Tony Anh Nguyen
    *  Student Number: s3721573
    */

    [SerializeField]
    private LevelManager _levelManager;
    private bool _canHit;

    private void Start()
    {
        _canHit = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_canHit == true)
        {
            //Freeze the ball and move the ball to the centre
            if (other.CompareTag("Ball"))
            {
                other.GetComponent<Rigidbody>().velocity = Vector3.zero;
                other.GetComponent<Rigidbody>().Sleep();
                StartCoroutine(Move(other.gameObject, 0.2f));
            }
            else if (other.CompareTag("Cube"))
            {
                _canHit = false;
                _levelManager.EndGame();
            }
        }
    }

    //Moves the ball to the centre within the time length
    private IEnumerator Move(GameObject ball, float timeLength)
    {
        float elapseTime = 0;

        Vector3 startPosition = ball.transform.position;
        while (elapseTime < timeLength)
        {
            elapseTime += Time.deltaTime;
            ball.transform.position = Vector3.Lerp(startPosition, Vector3.zero, elapseTime / timeLength);
            yield return null;
        }
        ball.transform.position = Vector3.zero;
        _levelManager.NextLevel();
    }
}
