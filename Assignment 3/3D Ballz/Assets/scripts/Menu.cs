﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

    [SerializeField]
    private GameObject _shop;
    [SerializeField]
    private GameObject _menu;

    //Changes scene to next scene through build index
    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    //Changes scene back to menu scene
    public void EndGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);

    }

    //method for opening shop through menu
    public void EnterShop()
    {
        _shop.SetActive(true);
        _menu.SetActive(false);
    }

    //Method for restarting game and resetting coins and unlockables
    public void Restart()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("Menu");
    }
}
