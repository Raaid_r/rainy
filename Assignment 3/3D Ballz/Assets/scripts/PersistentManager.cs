﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentManager : MonoBehaviour
{
    /* Author: Tony Anh Nguyen
    *  Student Number: s3721573
    */

    public static PersistentManager Player { get; private set; }

    [SerializeField]
    private int _coins;
    [SerializeField]
    private int[] _highScores;
    private int _skinID;
    [SerializeField]
    private Material _skinTexture;

    public Material SkinTexture
    {
        get { return _skinTexture; }
    }

    public int GetHighScore(int gameMode)
    {
        return _highScores[gameMode];
    }

    public int Coins
    {
        get
        {
            return _coins;
        }
    }

    private void Awake()
    {
        if (Player == null)
        {
            Player = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        //Get the coins, current skin and highscore from save data
        Player._coins = PlayerPrefs.GetInt("Coins");
        for (int i = 0; i < _highScores.Length; i++)
        {
            Player._highScores[i] = PlayerPrefs.GetInt("Highscore" + i);
        }
        Player._skinID = PlayerPrefs.GetInt("CurrentSkin");
        Player._skinTexture = ShopManager.Shop.GetMaterial(_skinID);
    }

    private void Start()
    {
        Coin.OnCollect += AddCoins;
    }

    public void AddCoins(int amount)
    {
        //add coin and save
        _coins += amount;
        PlayerPrefs.SetInt("Coins", _coins);
    }

    public bool CanSpend(int amount)
    {
        //Checks if the item is affordable
        if (amount > _coins)
        {
            return false;
        }
        return true;
    }

    public void SpendCoins(int amount)
    {
        //Can only spend amount if its affordable
        if (CanSpend(amount))
        {
            _coins -= amount;
            PlayerPrefs.SetInt("Coins", _coins);
        }
    }

    public void UpdateHighScore(int gameMode, int score)
    {
        //Makes sure the gamemode exists
        if (gameMode >= _highScores.Length)
        {
            Debug.Log("game mode out of range!");
            return;
        }
        //Ensures that highscore is kept as high as possible, and not lowered
        if (score > _highScores[gameMode])
        {
            _highScores[gameMode] = score;
            PlayerPrefs.SetInt("Highscore" + gameMode, score);
        }
    }

    public void SetSkin(Skin skin)
    {
        //set the skin to the texture
        _skinID = skin.ID;
        _skinTexture = skin.Material;
        PlayerPrefs.SetInt("CurrentSkin", _skinID);
    }

    public bool SkinEquals(Skin skin)
    {
        //checks if skin is the same skin as the one given
        if(_skinID == skin.ID)
        {
            return true;
        }
        return false;
    }
}
