﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerMotor : MonoBehaviour {

    /* Author: Tony Anh Nguyen
     * Student Number: s3721573
     */

    [SerializeField]
    private Camera _camera;
    [SerializeField]
    private BallMovement _ballMovement;
    [SerializeField]
    private GameObject _fastFowardUI;
    [SerializeField]
    private float _fastFowardSpeed;
    [SerializeField]
    private float _fastForwardTime;
    private const int _lowerVerticalLimit = 270;
    private const int _upperVerticalLimit = 330;
    private Vector3 _horizontalRotation;
    private Vector3 _verticalRotation;
    private float _time;
    private LineRenderer _lineRenderer;
    private Vector3 _position;

    public bool CanFastFoward
    {
        get
        {
            return _ballMovement.IsAllBallsShot && _time > _fastForwardTime ? true : false;
        }
    }

	private void Start ()
    {
        _horizontalRotation = Vector3.zero;
        _verticalRotation = Vector3.zero;
        _time = 0;
        _lineRenderer = GetComponent<LineRenderer>();

    }

    private void Update()
    {
        RaycastHit hit;
        Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hit);
        _lineRenderer.SetPosition(1, _camera.transform.forward * hit.distance);
        if (CanFastFoward && Time.timeScale == 1)
        {
            _fastFowardUI.SetActive(true);
        }
        else
        {
            _fastFowardUI.SetActive(false);
        }
    }

    private void FixedUpdate()
    {
        _time += Time.deltaTime;
        if (!_ballMovement.IsMoving)
        {
            _time = 0;
            Time.timeScale = 1;
        }
        Rotate();
    }

    public void SetHorizontalRotation(Vector3 horizontalRotation)
    {
        _horizontalRotation = horizontalRotation;

    }

    public void SetVerticalRotation(Vector3 verticalRotation)
    {
        _verticalRotation = verticalRotation;
    }

    private void Rotate()
    {
        //This is in charge of rotating horizontally
        transform.Rotate(_horizontalRotation, Space.World);
        if (_camera != null)
        {
            //Rotates the camera vertically, however limits it between lower limit and uppper limit degrees
            _camera.transform.Rotate(_verticalRotation);
            if (_camera.transform.rotation.eulerAngles.x < _lowerVerticalLimit 
                || _camera.transform.rotation.eulerAngles.x > _upperVerticalLimit)
            {
                _camera.transform.Rotate(-_verticalRotation);
            }
        }
    }

    public void Shoot()
    {
        _ballMovement.Move(_camera.transform.forward);
    }

    public void FastFoward()
    {
        if (_fastFowardSpeed <= 1)
        {
            Debug.Log("fast foward speed must be more than one");
        }
        else if(CanFastFoward)
        {
            Time.timeScale = _fastFowardSpeed;
        }
    }
}
