﻿using UnityEngine;
using UnityEngine.SceneManagement;
[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{
    /* Author: Tony Anh Nguyen
     * Student Number: s3721573
     */

    [SerializeField]
    private float _horizontalSpeed;
    [SerializeField]
    private float _verticalSpeed;

    private PlayerMotor _motor;

    private void Start()
    {
        _motor = GetComponent<PlayerMotor>();
        //Locks cursor to the window
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void Update()
    {
        //Get input from player's mouse
        float yRotation = Input.GetAxisRaw("Mouse X");
        float xRotation = Input.GetAxisRaw("Mouse Y");

        Vector3 verticalRotation = new Vector3(-xRotation, 0f, 0f) * _verticalSpeed;
        Vector3 horizontalRotation = new Vector3(0f, yRotation, 0f) * _horizontalSpeed;

        _motor.SetHorizontalRotation(horizontalRotation);
        _motor.SetVerticalRotation(verticalRotation);
        if (Input.GetButtonDown("Fire1"))
        {
            Debug.Log("Shoot");
            _motor.Shoot();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Speed");
            _motor.FastFoward();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
