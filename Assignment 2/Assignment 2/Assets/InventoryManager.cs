﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour {

    // Use this for initialization
    public Animator animator1;
    public Animator animator2;

    public void OpenInventory()
    {
        animator1.SetBool("IsOpen", true);
        animator2.SetBool("IsOpen", true);
    }

    public void CloseInventory()
    {
        animator1.SetBool("IsOpen", false);
        animator2.SetBool("IsOpen", false);
    }
}
