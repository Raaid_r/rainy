using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

// This class is used to manage the text that is
// displayed on screen.  In situations where many
// messages are triggered one after another it
// makes sure they are played in the correct order.
public class TextManager : MonoBehaviour
{
    // This struct encapsulates the messages that are
    // sent for organising.
    public struct Instruction
    {
        public string message;      // The body of the message.
        public Color textColor;     // The color the message should be displayed in.
        public float startTime;     // The time the message should start being displayed based on when it is triggered and its delay.
    }


    public Text text;                               // Reference to the Text component that will display the message.
    public GameObject innerPanel;                   // Inner panel for text box - Raaid
    public GameObject outerPanel;                   // Outer panel for text box - Raaid
    public float displayTimePerCharacter = 0.1f;    // The amount of time that each character in a message adds to the amount of time it is displayed for.
    public float additionalDisplayTime = 0.5f;      // The additional time that is added to the message is displayed for.
    public Dialogue dialogue;
    public string[] str = new string[1];
    private float _time = 0;
    private int _i = 0;
    private char[] _letters = new char[2] ;

    private List<Instruction> instructions = new List<Instruction> ();
                                                    // Collection of instructions that are ordered by their startTime.
    private float clearTime;                        // The time at which there should no longer be any text on screen.


    private void Update ()
    {
        // If there are instructions and the time is beyond the start time of the first instruction...
        if (instructions.Count > 0 && Time.time >= instructions[0].startTime)
        {
            dialogue.name = "hi";
            str[0] = instructions[0].message;
            dialogue.sentences = str;
            //// set the scrolling effect to the start when the instruction starts - Tony
            //_i = 0;
            //// ... set the Text component to display the instruction's message in the correct color.
            //_letters = instructions[0].message.ToCharArray();
            //text.text = "";
            //text.color = instructions[0].textColor;
            //innerPanel.GetComponent<Image>().color = new Color32(240, 240, 240, 255);       // Changes color of panels when text is shown - Raaid
            //outerPanel.GetComponent<Image>().color = new Color32(0, 0, 0, 230);
            FindObjectOfType<AnimationManager>().StartDialogue(dialogue, 0);
            // Then remove the instruction.
            instructions.RemoveAt (0);
        }
        // Otherwise, if the time is beyond the clear time, clear the text component's text, and reset
        // the time and index of the letters. - Tony
        else if (Time.time >= clearTime) 
        {
            text.text = string.Empty;
            innerPanel.GetComponent<Image>().color = Color.clear;                           // Clears text panel when text is over - Raaid
            outerPanel.GetComponent<Image>().color = Color.clear;
        }
        _time += Time.deltaTime;
        // For every 0.02 second and the text hasnt fully finished, add in a letter and reset the time to 0 - Tony
        if (_time > 0.02 && _i < _letters.Length)
        {
            text.text += _letters[_i++].ToString();
            _time = 0;
        }
    }


    // This function is called from TextReactions in order to display a message to the screen.
    public void DisplayMessage (string message, Color textColor, float delay)
    {
        // Set the scrolling effect back to the start of the sentence - Tony
        _i = 0;
        // The time when the message should start displaying is the current time offset by the delay.
        float startTime = Time.time + delay;

        // Calculate how long the message should be displayed for based on the number of characters.
        float displayDuration = message.Length * displayTimePerCharacter + additionalDisplayTime;

        // Create a new clear time...
        float newClearTime = startTime + displayDuration;

        // ... and if it is after the old clear time, replace the old clear time with the new.
        if (newClearTime > clearTime)
            clearTime = newClearTime;

        // Create a new instruction.
        Instruction newInstruction = new Instruction
        {
            message = message,
            textColor = textColor,
            startTime = startTime
        };

        // Add the new instruction to the collection.
        instructions.Add (newInstruction);

        // Order the instructions by their start time.
        SortInstructions ();
    }


    // This function orders the instructions by start time using a bubble sort.
    private void SortInstructions ()
    {
        // Go through all the instructions...
        for (int i = 0; i < instructions.Count; i++)
        {
            // ... and create a flag to determine if any reordering has been done.
            bool swapped = false;

            // For each instruction, go through all the instructions...
            for (int j = 0; j < instructions.Count; j++)
            {
                // ... and compare the instruction from the outer loop with this one.
                // If the outer loop's instruction has a later start time, swap their positions and set the flag to true.
                if (instructions[i].startTime > instructions[j].startTime)
                {
                    Instruction temp = instructions[i];
                    instructions[i] = instructions[j];
                    instructions[j] = temp;

                    swapped = true;
                }
            }

            // If for a single instruction, all other instructions are later then they are correctly ordered.
            if (!swapped)
                break;
        }
    }
}

