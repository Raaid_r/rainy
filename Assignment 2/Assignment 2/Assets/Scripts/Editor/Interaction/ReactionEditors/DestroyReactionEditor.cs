﻿using UnityEditor;

[CustomEditor(typeof(DestroyReaction))]
public class DestroyReactionEditor : ReactionEditor
{
    protected override string GetFoldoutLabel()
    {
        return "Destory Reaction";
    }
}
