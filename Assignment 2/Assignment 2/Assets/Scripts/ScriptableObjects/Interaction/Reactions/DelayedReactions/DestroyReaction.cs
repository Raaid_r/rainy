﻿using UnityEngine;

public class DestroyReaction : DelayedReaction {

    public GameObject gameObject;

    protected override void ImmediateReaction()
    {
        Destroy(gameObject);
    }
}
