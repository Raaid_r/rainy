﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropItem : MonoBehaviour {

    /*
        Author: Tony Anh Nguyen
    */

    public Item item;
    public DialogueTrigger dialogueTrigger;

    public void DropItemTrigger()
    {
        string[] strs;
        if (GameObject.Find("SecurityRoom") == null)
        {
            if (item != null)
            {
                ItemDrop();
            }
        }
        else
        {
            dialogueTrigger.dialogue.name = "Conscious";
            dialogueTrigger.dialogue.sentences = new string[1];
            dialogueTrigger.dialogue.sentences[0] = "Should probably not litter inside...";
            dialogueTrigger.TriggerDialogue();
        }
    }

    public void SetItem(GameObject button)
    {
        button.GetComponent<DropItem>().item = item;
    }

    private void ItemDrop()
    {

        GameObject interactable = GameObject.Find(item.name + "Interactable");
        if (interactable != GameObject.Find("DirtyBananaInteractable"))
        {
            Reaction[] reactions = interactable.GetComponentInChildren<ReactionCollection>().reactions;
            GameObject player = GameObject.Find("Player");
            GameObject gameObject;
            for (int i = reactions.Length - 1; i > 0; i--)
            {
                if (reactions[i] is GameObjectReaction)
                {
                    gameObject = ((GameObjectReaction)reactions[i]).gameObject;
                    if (gameObject.name.Equals(item.name))
                    {
                        gameObject.transform.position = player.transform.position;
                    }
                    gameObject.SetActive(!((GameObjectReaction)reactions[i]).activeState);
                }
                else if (reactions[i] is ConditionReaction)
                {
                    ((ConditionReaction)reactions[i]).condition.satisfied = !((ConditionReaction)reactions[i]).satisfied;
                }
                else if (reactions[i] is AnimationReaction)
                {
                    if (((AnimationReaction)reactions[i]).trigger.Equals("MedTake") ||
                       ((AnimationReaction)reactions[i]).trigger.Equals("HighTake"))
                    {
                        ((AnimationReaction)reactions[i]).trigger = "LowTake";
                    }
                }
            }
            interactable.transform.Find("InteractionLocation").transform.position = interactable.transform.position;
            interactable.transform.position = player.transform.position;
            interactable.GetComponent<BoxCollider>().center = Vector3.zero;
            this.gameObject.transform.root.GetComponent<Inventory>().RemoveItem(item);
        }
        else
        {
            dialogueTrigger.dialogue.name = "Conscious";
            dialogueTrigger.dialogue.sentences = new string[1];
            dialogueTrigger.dialogue.sentences[0] = "I probably shouldn't drop this...";
            dialogueTrigger.TriggerDialogue();
        }
    }
}
