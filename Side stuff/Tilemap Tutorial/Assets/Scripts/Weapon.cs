﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : ScriptableObject
{
    [SerializeField]
    protected float _dmg;
    [SerializeField]
    protected float _speed;

    public float Speed
    {
        get { return _speed; }
    }

    public abstract IEnumerator Shoot(Vector2 direction, Transform firepoint);
}
