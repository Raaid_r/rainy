﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerShoot))]

public class PlayerController : MonoBehaviour {

    private PlayerMovement _movement;
    private PlayerShoot _shoot;

	void Start ()
    {
        _movement = GetComponent<PlayerMovement>();
        _shoot = GetComponent<PlayerShoot>();
	}
	
	void Update ()
    {
        float xMovement = Input.GetAxisRaw("Horizontal");
        float yMovement = Input.GetAxisRaw("Vertical");
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); ;


        Vector2 velocity = _movement.CalculateVelocity(xMovement, yMovement);

        _movement.Move(velocity);

        Vector2 direction = _movement.CalculateDirection(mousePosition);
        if (Input.GetButton("Fire1"))
        {
            _shoot.Shoot(direction);
        }
    }
}
