﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
    [SerializeField]
    private Weapon _weapon;
    [SerializeField]
    private Transform _firepoint;
    [SerializeField]
    private bool _canShoot;

    private void Start()
    {
        _canShoot = true;
    }

    public void Shoot(Vector2 direction)
    {
        if (_canShoot == true)
        {
            StartCoroutine(_weapon.Shoot(direction, _firepoint));
            StartCoroutine(Wait(_weapon.Speed));
        }
    }

    IEnumerator Wait(float time)
    {
        _canShoot = false;
        yield return new WaitForSeconds(time);
        _canShoot = true;
    }

}
