﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float _speed;

    private Rigidbody2D _rigidBody;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();   
    }

    public void Move(Vector2 velocity)
    {
        _rigidBody.MovePosition(_rigidBody.position + velocity * Time.deltaTime);
    }

    public Vector2 CalculateVelocity(float xMovement, float yMovement)
    {
        Vector2 horizontalMovement = transform.right * xMovement;
        Vector2 verticalMovement = transform.up * yMovement;

        return (horizontalMovement + verticalMovement).normalized * _speed;
    }

    public Vector2 CalculateDirection(Vector2 mousePosition)
    {
        Vector2 direction = (mousePosition - (Vector2)transform.position).normalized;
        return direction;
    }
}
