﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
public class MovementController : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _smoothing;

    private Rigidbody2D _rigidBody;
    private Vector2 _velocity;
    private float _time;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    public void Move(Vector2 direction)
    {
        _velocity = direction * _speed;
        _time = 0;
    }

    private void Update()
    {
        _time += Time.deltaTime;
        if (_time < _smoothing)
        {
            _rigidBody.velocity = Vector2.Lerp(_rigidBody.velocity, _velocity, _time / _smoothing);
        }
        else
        {
            _rigidBody.velocity = _velocity;
        }
    }

    public Vector2 Direction
    {
        get { return _rigidBody.velocity.normalized; }
    }
}
