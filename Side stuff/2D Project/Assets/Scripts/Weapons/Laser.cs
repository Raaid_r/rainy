﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Laser")]
public class Laser : Weapon
{
    [SerializeField]
    private GameObject _laserPrefab;

    public override IEnumerator Shoot(Vector2 direction, Transform firepoint)
    {
        GameObject laser = Instantiate(_laserPrefab);
        LineRenderer line = laser.GetComponent<LineRenderer>();
        RaycastHit2D hitInfo = Physics2D.Raycast(firepoint.position, direction);

        line.SetPosition(0, firepoint.position);
        if (hitInfo)
        {
            line.SetPosition(1, direction * hitInfo.distance + (Vector2)firepoint.position);
        }
        else
        {
            line.SetPosition(1, direction * 100 + (Vector2)firepoint.position);
        }


        laser.SetActive(true);

        yield return 0;

        laser.SetActive(false);
    }
}
