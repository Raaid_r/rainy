﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : ScriptableObject
{
    [SerializeField]
    protected float _dmg;
    [SerializeField]
    protected float _fireRate;

    public float FireRate
    {
        get { return _fireRate; }
    }

    public abstract IEnumerator Shoot(Vector2 direction, Transform firepoint);
}
