﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Weapon", menuName = "Bullet")]
public class Gun : Weapon
{
    [SerializeField]
    private GameObject _bulletPrefab;

    public override IEnumerator Shoot(Vector2 direction, Transform firepoint)
    {
        GameObject bullet = Instantiate(_bulletPrefab, firepoint.position, firepoint.rotation);
        bullet.GetComponent<Rigidbody2D>().velocity = direction;
        yield return null;
    }
}
