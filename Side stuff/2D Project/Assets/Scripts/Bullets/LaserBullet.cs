﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserBullet : Bullet
{
    private void Start()
    {
        Vector3 direction = (GetComponent<LineRenderer>().GetPosition(1) - GetComponent<LineRenderer>().GetPosition(0)).normalized;
        if(_impactEffect != null)
            Instantiate(_impactEffect, GetComponent<LineRenderer>().GetPosition(1), Quaternion.identity).transform.up = direction;
    }
}
