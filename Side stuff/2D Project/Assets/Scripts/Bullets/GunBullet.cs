﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class GunBullet : Bullet
{
    [SerializeField]
    private float _speed;
    private Rigidbody2D _rigidbody;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _rigidbody.velocity *= _speed;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_impactEffect != null)
            Instantiate(_impactEffect, transform.position, transform.rotation);
        gameObject.SetActive(false);
    }

}
