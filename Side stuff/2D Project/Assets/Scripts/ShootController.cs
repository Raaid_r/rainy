﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour
{
    [SerializeField]
    private Weapon _weapon;
    [SerializeField]
    private Transform _firepoint;
    private bool _canShoot;

    public Weapon Weapon
    {
        set { _weapon = value; }
    }

    private void Start()
    {
        _canShoot = true;
    }

    public void Shoot(Vector2 direction)
    {

        if (_canShoot == true)
        {
            StartCoroutine(_weapon.Shoot(direction, _firepoint));
            StartCoroutine(Wait(1/_weapon.FireRate));
        }
    }

    IEnumerator Wait(float time)
    {
        _canShoot = false;
        yield return new WaitForSeconds(time);
        _canShoot = true;
    }

}
