﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager Instance { get; private set; }

    [SerializeField]
    private Weapon[] _weapons;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddWeapon(Weapon weapon)
    {
        for (int i = 0; i < _weapons.Length; i++)
        {
            if (_weapons[i] == null)
            {
                _weapons[i] = weapon;
                return;
            }
        }
        Debug.Log("Inventory Full");
    }

    public void RemoveWeapon(Weapon weapon)
    {
        for (int i = 0; i < _weapons.Length; i++)
        {
            if (_weapons[i] == weapon)
            {
                _weapons[i] = null;
                return;
            }
        }
        Debug.Log("No such weapon");
    }

    public Weapon GetWeapon(int index)
    {
        if (index < 0 || index >= _weapons.Length)
        {
            Debug.Log("index for weapon out of range");
            return null;
        }
        return _weapons[index];
    }

}
