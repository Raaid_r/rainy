﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    private RaycastHit2D _frontLeft;
    private RaycastHit2D _left;
    private RaycastHit2D _right;
    private RaycastHit2D _frontRight;
    private RaycastHit2D _backLeft;
    private RaycastHit2D _backRight;

    private MovementController _movement;

    [SerializeField]
    private Vector2 _sensorPlacement;
    [SerializeField]
    [Range(0, 10)]
    private float _sensorRange;
    [SerializeField]
    private float _sensorDistance;
    [SerializeField]
    private float _backSensorPlacement;

    public RaycastHit2D Left
    {
        get { return _left; }
    }
    public RaycastHit2D Right
    {
        get { return _right; }
    }
    public RaycastHit2D BackRight
    {
        get { return _backRight; }
    }
    public RaycastHit2D BackLeft
    {
        get { return _backLeft; }
    }




    private void Start()
    {
        _movement = GetComponent<MovementController>();
    }

    private void Update()
    {
        Vector2 perpendicular = Vector2.Perpendicular(_movement.Direction);
        Vector2 leftDirection = (_movement.Direction + perpendicular * _sensorRange).normalized;
        Vector2 rightDirection = (_movement.Direction - perpendicular * _sensorRange).normalized;
        Vector2 leftSensorPos = (Vector2)transform.position + perpendicular * _sensorPlacement.x + _movement.Direction * _sensorPlacement.y;
        Vector2 rightSensorPos = (Vector2)transform.position - perpendicular * _sensorPlacement.x + _movement.Direction * _sensorPlacement.y;
        Vector2 backSensorPos = (Vector2)transform.position - _backSensorPlacement * _movement.Direction;

        if (_left = Physics2D.Raycast(leftSensorPos, leftDirection, _sensorDistance))
        {
            Debug.DrawRay(leftSensorPos, leftDirection * _sensorDistance, Color.red);
        }
        else
        {
            Debug.DrawRay(leftSensorPos, leftDirection * _sensorDistance, Color.yellow);
        }

        if (_right = Physics2D.Raycast(rightSensorPos, rightDirection, _sensorDistance))
        {
            Debug.DrawRay(rightSensorPos, rightDirection * _sensorDistance, Color.red);
        }
        else
        {
            Debug.DrawRay(rightSensorPos, rightDirection * _sensorDistance, Color.yellow);
        }

        if (_backLeft = Physics2D.Raycast(backSensorPos, perpendicular, _sensorDistance))
        {
            Debug.DrawRay(backSensorPos, perpendicular * _sensorDistance, Color.red);
        }
        else
        {
            Debug.DrawRay(backSensorPos, perpendicular * _sensorDistance, Color.yellow);
        }

        if (_backRight = Physics2D.Raycast(backSensorPos, -perpendicular, _sensorDistance))
        {
            Debug.DrawRay(backSensorPos, -perpendicular * _sensorDistance, Color.red);
        }
        else
        {
            Debug.DrawRay(backSensorPos, -perpendicular * _sensorDistance, Color.yellow);
        }
    }
}
