﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(MovementController))]
[RequireComponent(typeof(ShootController))]

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private GameObject _crossHair;

    private MovementController _movement;
    private ShootController _shoot;

    public Weapon weapon1;
    public Weapon weapon2;

	void Start ()
    {
        _movement = GetComponent<MovementController>();
        _shoot = GetComponent<ShootController>();
	}
	
	void Update ()
    {
        float xMovement = Input.GetAxisRaw("Horizontal");
        float yMovement = Input.GetAxisRaw("Vertical");
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = new Vector2(xMovement, yMovement).normalized;

        _movement.Move(direction);

        direction = (mousePosition - (Vector2)transform.position).normalized;
        
        _crossHair.transform.up = direction;
        if (Input.GetButton("Fire1"))
        {
            _shoot.Shoot(direction);
        }
    }
}
