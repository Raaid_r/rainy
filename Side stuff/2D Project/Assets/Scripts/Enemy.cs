﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MovementController))]
public class Enemy : MonoBehaviour {


    private MovementController _movement;
    private Sensor _sensor;

    [SerializeField]
    private Transform _target;

	private void Start ()
    {
        _movement = GetComponent<MovementController>();
        _sensor = GetComponent<Sensor>();
	}

    public void Follow()
    {
        Vector2 direction = (_target.position - transform.position).normalized;
        _movement.Move(direction);
        if (_sensor.Right && !_sensor.Right.transform.name.Equals("Enemy"))
        {
            Debug.Log("Right");
            direction = -Vector2.Perpendicular(_sensor.Right.normal);
            _movement.Move(direction);
        }
        else if (_sensor.Left && !_sensor.Left.transform.name.Equals("Enemy"))
        {
            Debug.Log("Left");
            direction = Vector2.Perpendicular(_sensor.Left.normal);
            _movement.Move(direction);
        }
    }

    public void Avoid()
    {
        Vector2 direction = _movement.Direction;
        RaycastHit2D hit = Physics2D.Raycast((Vector2)transform.position + direction, direction, 4);
        direction = (direction + hit.normal).normalized;
        _movement.Move(direction);
    }
}
