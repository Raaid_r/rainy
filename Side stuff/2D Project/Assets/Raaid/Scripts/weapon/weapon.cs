﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Weapon")]
public class weapon : ScriptableObject {

    public new string name;
    //public string description;

    public string type;
    public int damagePerShot;
    public int ammo;



}
