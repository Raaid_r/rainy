﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class weaponController : MonoBehaviour {

    public weapon weapon;

    public GameObject playerObj;
    private Transform firePoint;
    public GameObject bulletPrefab;
    private int ammo;
    private int damage;
    private string type;


    private void Start()
    {
        
        ammo = weapon.ammo;
        damage = weapon.damagePerShot;
        type = weapon.type;
    }

    void Update ()
    {
        firePoint = playerObj.transform.GetComponentInChildren<Transform>();
        if (ammo <= 0)
        {
            Destroy(gameObject);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            if (type.Equals("Bullet"))
            {
                ShootBullet();
            }
            if (type.Equals("Beam"))
            {
                ShootBeam();
            }
            
        }

	}


    void ShootBullet()
    {
        ammo--;
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
    }

    void ShootBeam()
    {
        ammo--;
        RaycastHit2D hitInfo = Physics2D.Raycast(firePoint.position, firePoint.right);

        if (hitInfo)
        {
            enemyController enemy = hitInfo.transform.GetComponent<enemyController>();
            if (enemy != null)
            {
                enemy.OnTakeDamage(damage);
            }
        }

    }
}
