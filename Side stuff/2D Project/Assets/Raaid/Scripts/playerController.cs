﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController : MonoBehaviour {

    public int hp = 100;
    public GameObject[] weapons;

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        enemyController target = hitInfo.GetComponent<enemyController>();
        if (target != null)
        {
            hp = hp - 5;
        }

    }

}
