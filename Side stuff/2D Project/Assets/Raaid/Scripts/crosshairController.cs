﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crosshairController : MonoBehaviour {

    public float aimSpeed = 600f;

    private float movement;

    public GameObject playerObject;

    private void Update()
    {
        FaceMouse();
    }

    void FaceMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);

        Vector2 direction = new Vector2(mousePosition.x - playerObject.transform.position.x, mousePosition.y - playerObject.transform.position.y);

        

        transform.up = direction;
    }

}
