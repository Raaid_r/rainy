﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerMovement : MonoBehaviour {

    public float _playerSpeed;
    private int newDirX = 0;
    private int newDirY = 0;


    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        _playerSpeed = 5f;
    }

    void Update ()
    {
        CheckUserInput();
        MoveObject();
    }



    private void CheckUserInput()
    {

        if (Input.GetKey(KeyCode.W))
        {
            newDirY = 1;
        }
        if (Input.GetKey(KeyCode.S))
        {
            newDirY = -1;
        } 
        if (Input.GetKey(KeyCode.A))
        {
            newDirX = -1;
        }
        if (Input.GetKey(KeyCode.D))
        {
            newDirX = 1;
        }


        if (Input.GetKeyUp(KeyCode.W))
        {
            newDirY = 0;
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            newDirY = 0;
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            newDirX = 0;
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            newDirX = 0;
        }

    }


    private void MoveObject()
    {
        Vector2 currentPos = gameObject.transform.position;

        gameObject.transform.position = new Vector2(gameObject.transform.position.x + newDirX * _playerSpeed * Time.deltaTime, gameObject.transform.position.y + newDirY * _playerSpeed * Time.deltaTime);

    }




}
